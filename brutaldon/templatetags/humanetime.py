from datetime import datetime, timedelta
from django.utils.timezone import get_default_timezone, get_current_timezone, localtime
from django.utils.timezone import now as django_now
from django.utils.translation import gettext as _
from django import template

register = template.Library()


@register.filter(is_safe=True)
def humane_time(arg):
    """Returns a time string that is humane but not relative (unlike Django's humanetime)

    It is not safe to use on future times.

    """
    now = django_now()
    arg = localtime(arg)
    diff = now - arg

    if arg.tzinfo == now.tzinfo:
        utc = " (UTC)"
    else:
        utc = ""
    return arg.strftime("%b %d, %Y, %I:%M %p") + utc
